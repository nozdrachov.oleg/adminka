## Adminka

## Установка и настройка

1. **Клонирование репозитория**
- `https://gitlab.com/nozdrachov.oleg/adminka.git`

2. **Настройка Docker**

Установите [Docker](https://www.docker.com/) и [Docker Compose](https://docs.docker.com/compose/), если у вас их еще нет.

3. **Настройка переменных окружения**

-Создайте файл `.env` в корне проекта и настройте его по примеру с .env.example:
(Заполните значения переменных окружения в соответствии с вашими настройками.)

4. **Запуск контейнеров Docker**
- `docker compose up --build -d`

5.Установите зависимости в контейнер :
- `docker compose run --rm php-cli-container composer install`
-  `docker compose run --rm node-container npm install`

6. **Накатите миграцию базы данных**

Накатите миграцию базы данных с помощью следующей команды :
- `docker compose run --rm php-cli-container php artisan migrate`

7.Накатите Сиды:
-`docker compose run --rm php-cli-container php artisan db:seed`

8. **Проверьте подключение к базе данных**
- если у вас не получается , попробуйте удалить `невидимого пользователя - (потушите контейнера  , а не образы`

9. **Проверка работы приложения**

Откройте веб-браузер и перейдите по адресу `http://localhost:8080/`. Вы должны увидеть рабочее приложение.

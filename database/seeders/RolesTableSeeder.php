<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id' => 1,
                'lvl' => '1',
            ],
            [
                'id' => 2,
                'lvl' => '2',
            ]
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }

    }
}

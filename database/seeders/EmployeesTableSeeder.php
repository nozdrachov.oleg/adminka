<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach(range(1, 500) as $index) {

            // Используем Faker для генерации данных
            $name = $faker->name;
            $position = $faker->jobTitle;
            $date = $faker->date;
            $phone = $faker->phoneNumber;
            $email = $faker->unique()->safeEmail;  // unique() гарантирует уникальность email
            $salary = $faker->randomFloat(2, 3000, 9000);  // Случайное число с 2 десятичными знаками, между 3000 и 9000
            $image = $faker->imageUrl(640, 480, 'people');  // Просто пример, здесь можно использовать свои пути к изображениям

            // Вставляем данные в таблицу
            DB::table('employees')->insert([
                'name' => $name,
                'position' => $position,
                'date' => $date,
                'phone' => $phone,
                'email' => $email,
                'salary' => $salary,
                'image' => $image,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>My App</title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
<div id="app">
    <!-- Your Vue.js application will mount here -->
</div>

<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>

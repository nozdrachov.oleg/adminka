import Vue from 'vue';
import Router from 'vue-router';
import AdminAuth from '../components/AdminAuth.vue';
import AdminPanel from '../components/AdminPanel.vue';
import store from '../store/index';

Vue.use(Router);

const router = new Router({
    mode: "history",
    routes: [
        {
            path: '/',
            name: 'AdminAuth',
            component: AdminAuth,
            meta: {guestOnly : true}
        },
        {
            path: '/admin',
            name: 'AdminPanel',
            component: AdminPanel,
            meta: { requiresAuth: true } // Добавляем мета-тег
        }
    ]
});

router.beforeEach(async (to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const guestOnly = to.matched.some(record => record.meta.guestOnly);

    if (!store.state.auth.tokenChecked) {
        await store.dispatch('auth/checkToken');
        store.commit('auth/setTokenChecked', true);
    }

    if (requiresAuth && !store.getters['auth/isAuthenticated']) {
        next('/');
    } else if (guestOnly && store.getters['auth/isAuthenticated']) {
        next('/admin');
    } else {
        next();
    }
});

export default router;

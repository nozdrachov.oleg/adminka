import axios from "axios";

const adminPanel = {
    namespaced: true,
    state: {
        users: {},
        pagination: {},
        currentPage: 1,
        lastPage: 0
    },
    getters: {
        users: state => state.users,
        currentPage: state => state.currentPage,
        lastPage: state => state.lastPage,
    },
    mutations: {
        setUsers(state, { data, ...pagination }) {
            state.users = data;
            state.pagination = pagination;
        },
        setCurrentPage(state, page) {
            state.currentPage = page;
        },
        setLastPage(state, lastPage) {
            state.lastPage = lastPage;
        },
        setDeleteUser(state, id) {
            state.users = state.users.filter(t => t.id !== id);
        }
    },
    actions: {
        async showUsers({ commit, state }, page = state.currentPage) {
            try {
                console.log('Current Page:', page);
                const response = await axios.get(`/api/adminPanel/showUsers?page=${page}`);
                if (response.status === 200 && response.data.users) {
                    commit('setUsers', response.data.users);
                    // commit('setCurrentPage', response.data.users.current_page);
                    commit('setLastPage', response.data.users.last_page);
                } else {
                    console.log("не получили данные с сервера");
                }
            } catch (error) {
                console.log(error);
            }
        },
        async deleteUser({commit}, id) {
            try {
                const response = await axios.delete(`/api/adminPanel/deleteUser/${id}`);
                if (response.status === 204) {
                    commit('setDeleteUser', id);
                }
            } catch (error) {
                console.log(error);
            }
        },
        async editUser({commit}, id) {
            try {
                const response = await axios.post(`/api/adminPanel/editUser?id=${id}`);
            } catch (error) {
                console.log(error);
            }
        },
        async addUser({commit}, user) {
            try {
                const response = await axios.post(`/api/adminPanel/addUser`, user);
                if (response.status === 201 && response.data.users) {
                    commit('setUsers', response.data.users);
                    commit('setLastPage', response.data.users.last_page);
                }
            } catch (error) {
                console.log(error);
            }
        }
    }
}

export default adminPanel;

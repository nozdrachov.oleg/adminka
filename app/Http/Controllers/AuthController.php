<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use PHPOpenSourceSaver\JWTAuth\Exceptions\JWTException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\TokenExpiredException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\TokenInvalidException;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth:api', ['except' => ['login', 'register', 'checkToken']]);
    }

    public function checkToken(Request $request)
    {
        try {
            $token = $request->cookie('jwt_access');
            $isValid = JWTAuth::setToken($token)->check();
            if (!$isValid) {
                $token = JWTAuth::refresh($token);
            }

            $userId = JWTAuth::setToken($token)->getPayload()->get('sub');
            $userRoleLevel = Cache::get("user_role_{$userId}");
            if ($userRoleLevel === null) {
                throw new JWTException();
            }
            $accessCookie = cookie('jwt_access', $token, 1440, null, null, false, true);

            return response()->json([
                'valid' => true,
                'lvl' => $userRoleLevel,
            ])->withCookie($accessCookie);
        } catch (TokenExpiredException $e) {
            Log::error('Token Expired: ' . $e->getMessage());
            // время обновления токена истекло, нужна повторная аутентификация
            $expiredCookie = cookie('jwt_access', null, -1); // удаляем недействительный токен
            return response()->json(['valid' => false], 401)->withCookie($expiredCookie);

        } catch (TokenInvalidException $e) {
            // токен недействителен, нужна повторная аутентификация (НЕ ЗАЙДЕМ СЮДА ОСТАВИЛ ДЛЯ СЛУЧАЯ ЕСЛИ ПЕРЕПИСЫВАТЬ
            //ЛОГИКУ И ТОКЕН НЕ НУЖНО БУДЕТ ОБНОВЛЯТЬ)
            $expiredCookie = cookie('jwt_access', null, -1); // удаляем недействительный токен
            return response()->json(['valid' => false], 401)->withCookie($expiredCookie);
        } catch (JWTException $e) {

            // общая ошибка JWT, нужна повторная аутентификация
            $expiredCookie = cookie('jwt_access', null, -1); // удаляем недействительный токен
            return response()->json(['valid' => false], 401)->withCookie($expiredCookie);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login' => 'required|string',
            'password' => 'required|string|min:8|max:16|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors()
            ], 422);
        }

        $credentials = $request->only('login', 'password');
        //$token = auth()->attempt($credentials) - можно не через фасад как было ниже , а через помошника(хелпера)
        $token = Auth::attempt($credentials);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized',
            ], 401);
        }

        $accessCookie = Cookie::make('jwt_access', $token, 1440, null, null, true, true);
        $user = Auth::user();
        $user->load('role');
        Cache::put("user_role_{$user->id}", $user->role->lvl, 10080); // 1440 минут = 24 часа

        return response()->json([
            'status' => 'success',
            'auth' => true,
            'lvl' => $user->role->lvl,
        ])->withCookie($accessCookie);
    }

    public function register(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $token = Auth::login($user);
        return response()->json([
            'status' => 'success',
            'message' => 'User created successfully',
            'user' => $user,
            'authorisation' => [
                'token' => $token,
                'type' => 'bearer',
            ]
        ]);
    }

    public function logout(Request $request)
    {
        $token = $request->cookie('jwt_access');
        auth('api')->setToken($token)->invalidate();
        $expiredCookie = cookie('jwt_access', null, -1);
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out',
            ])->withCookie($expiredCookie);
    }

    public function refresh(Request $request)
    {
//        try {
//            $token = $request->cookie('jwt_access');
//            $newToken = JWTAuth::refresh($token);
//        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
//            return response()->json(['error' => 'token_expired'], 401);
//        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
//            return response()->json(['error' => 'token_invalid'], 401);
//        }
    }

}

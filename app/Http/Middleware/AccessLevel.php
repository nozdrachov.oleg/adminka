<?php

namespace App\Http\Middleware;

use Closure;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;

class AccessLevel
{
    public function handle(Request $request, Closure $next, $requiredLevel)
    {
        $token = $request->cookie('jwt_access');
        $user = JWTAuth::setToken($token)->authenticate();

        if (!$user) {
            return response()->json(['valid' => false], 404);
        }

        $role = $user->role()->first();

        if($role->lvl < $requiredLevel) {
            return response()->json(['valid' => false], 401);
        }

        return $next($request);
    }
}
